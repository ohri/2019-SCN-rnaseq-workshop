# 2019 Stem Cell Network RNASeq Workshop

## Workshop Dates
October 16-18, 2019


## Workshop Locations

### Day 1 & 2
University of Ottawa Main Campus  
Lamoureux Hall (LMX)  
145 Jean-Jacques Lussier, Room 219  
[Campus Map](https://www.uottawa.ca/facilities/sites/www.uottawa.ca.facilities/files/2018facultymap.pdf)  

### Day 3 
General Hospital Campus  
501 Smyth Road, Ottawa Ontario  
Critical Care Wing  
Room W1494  

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

[Workshop Agenda](2019RNA-SeqAnalysisWorkshopAgenda.pdf?inline=false) _(Updated Oct 15, 2019)_

### Day 1:

Workshop and Goals (Dr. Bill Stanford) [PowerPoint](Files/2019_Workshop_Stanford_Overview_And_Goals.pptx)

RNA-seq Basics (Gareth Palidwor) [PowerPoint](Files/2019_Workshop_RNASeq_Basics.pptx)
* Example FASTQC files
    * [ERR975344.1_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.1_fastqc.html?inline=false)
    * [ERR975344.2_fastqc](https://www.ogic.ca/projects/workshop_2019/FastQC/ERR975344.2_fastqc.html?inline=false)
* Example RNASEQC files
    * [Activated.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Activated.1/)
    * [Quiescent.1](http://www.ogic.ca/projects/workshop_2019/RNA-SeQC/Quiescent.1/)
* Example BAM files for IGV Exercise
    * [in_day1_rep1_chr12.bam](Files/in_day1_rep1_chr12.bam?inline=false)
    * [in_day1_rep1_chr12.bam.bai](Files/in_day1_rep1_chr12.bam.bai?inline=false)
* [R package installation script](Files/install_packages.R)
* [IGV instructions](Files/IGV.md)

Differential Gene Expression with DESeq2 (Chris Porter) [PowerPoint](Files/2019_Workshop_Lecture_2a-DESeq2.pptx)
* [Salmon gene counts for Differential Gene Expression Analysis](http://www.ogic.ca/projects/2020_RNASeq_Workshop/RNA-seq_salmon_files.zip)
* [R Script for Diffferential Gene Expression Analysis](Files/analysis_script.Rmd?inline=false)
* [Ranked significan gene list output g:Profiler exercise](Files/Ensembl_Act_vs_Qui_sig05_salmon.txt?inline=false)
* [Full ranked gene list ouptput for GSEA exercise](Files/Ensembl_Act_vs_Qui_all_salmon.txt?inline=false)

RNA-seq Experimental Design  (Dr. Ted Perkins) [PDF](Files/Perkins_RNASeqWorkshop_2019.pdf?inline=false)

### Day 2:

Gene Set Enrichment Analysis (Gareth Palidwor) [PowerPoint](Files/2019_Workshop_GeneAnnotationEnrichmentAnalysis.ppt)
* [g:Profiler instructions](Files/gprofiler.md)


Single Cell RNA-seq (David Cook) [PowerPoint](http://www.ogic.ca/projects/2019_SCN_RNASeq_workshop/DavidCook_scRNASeq.pptx) _UPDATED OCT 18, 2019_
* [filtered_gene_bc_matrices.zip](http://www.ogic.ca/projects/2019_SCN_RNASeq_workshop/filtered_gene_bc_matrices.zip)
* [Gitlab site for scRNA scripts](https://github.com/dpcook/scrna_seq_workshop_2019) _UPDATED OCT 18, 2019_





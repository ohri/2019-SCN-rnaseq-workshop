# Transcript quantification using Salmon

## Overview and links

Salmon is a tool to rapidly quantify transcripts in an RNA-seq experiment from sequence reads in fastq format.

The project home page on github can be found here:

* https://combine-lab.github.io/salmon/

An overview of the tools is here:

* https://combine-lab.github.io/salmon/about/

...and a 'getting started' tutorial is here:

* https://combine-lab.github.io/salmon/getting_started/

This last link should be sufficient to learn how to download and install salmon, generate an index, and quantify transcripts. 

Full documentation for the tools is here:

* https://salmon.readthedocs.io/en/latest/


## Notes

The salmon documentation recommends installing using conda, but it is also possible to download the
compressed binaries directly [from here](https://github.com/COMBINE-lab/salmon/releases/download/v1.1.0/salmon-1.1.0_linux_x86_64.tar.gz),
then extract the salmon directory with tar (`tar -xzf salmon-1.1.0_linux_x86_64.tar.gz`).

We typically align to the [GENCODE](http://www.gencodegenes.org/) transcript assembly. Current (April 2020)
transcript fastq files can be downloaded for [human release 33](ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_33/gencode.v33.transcripts.fa.gz)
and [mouse release M24](ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_mouse/release_M24/gencode.vM24.transcripts.fa.gz).
These are the files required to build the salmon index, using the command:

`salmon index --gencode -p 4 -t gencode.vM24.transcripts.fa -i GRCm38_GENCODE_M24`

Where 
* `-p 4` specifies to use four CPU cores to build the index (adjust as appropriate for the computer you're running on - 
more cores are faster if you have them and enough memory) 
* `-t gencode.vM24.transcripts.fa` specifies the file of transcript sequences (fastq, downloaded from GENCODE)
* `-i GRCm38_GENCODE_M24` specifies the name of the index to be built

I think that the fasta file containing transcript sequences needs to be uncompressed before indexing. This is
done with gunzip `gunzip gencode.vM24.transcripts.fa`. After the index is successfully built this file is no longer
required.

Once the index is built, the command to quantify transcripts from a fastq file is:

`salmon quant -p 4 -i GRCm38_GENCODE_M24 -l A --gcBias --validateMappings -r <FASTQ_FILE> -o <OUTPUT_DIR>`

* `-p 4` - number of CPU cores to use
* `-i GRCm38_GENCODE_M24` - name of (path to) the salmon index (same name used when indexing)
* `-r <FASTQ_FILE>` - replace `<FASTQ_FILE> with the (path to the) fastq file containing the RNA-seq reads
* `-o <OUTPUT_DIR>` - replace `<OUTPUT_DIR>` with the path to the directory in which to place the results. The directory will be created if it doesn't exist.

Files of RNA-seq reads can be either compressed (ending in `.gz`) or uncompressed. If you have paired-end reads, you need to specify two fastq files with `-1` and `-2`

`salmon quant -p 4 -i GRCm38_GENCODE_M24 -l A --gcBias --validateMappings -1 <FASTQ_FILE_READ_1> -2 <FASTQ_FILE_READ_2> -o <OUTPUT_DIR>`

## Example scripts

These scripts can be modified to run salmon indexing and alignment on a cluster running the Slurm queue manager 
(such as Compute Canada clusters). The file needs to be saved as a text file (can right click and select 'download file')
and uploaded to the user's cluster account.

The script needs to be edited - hopefully comments in the script will make this clear. Selections in angle brackets
`<>` need to be replaced, so `<path_to_salmon_index_dir>` should be replaced with the location of the directory
in which you want to write the indexes (_e.g._ `/home/cporter/salmon_indexes`) and `<index_name>` should be replaced
with the name to be used for the index (_e.g._ `mouse_GENCODE_24`).

After the file is edited and saved to the cluster, it can be added to the queue with:

`sbatch /home/cporter/index_salmon_script.sh`

(change path to the script as appropriate). It will then be run when a node becomes available, with output and errors
written to the file specified in the script headers (`#SBATCH -o <path_to_directory>/index_salmon.sh.%A_%a.stdout`).

The status of the queue can be viewed with `squeue`; to see only your own jobs in the queue use `squeue -u <username>`.

* [Script to index sequences (fasta file) on Compute Canada cluster](index_salmon.sh)
* [Script to run salmon alignment](run_salmon.sh)

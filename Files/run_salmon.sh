#!/bin/bash

#SBATCH -c 4			## The number of CPU cores to use. Stored in the variable $SLURM_JOB_CPUS_PER_NODE
#SBATCH --mem=8192		## Amount of memory to use in kb.
#SBATCH --array=1-2		## Which tasks to run (see lines 29-36). Can be a range (1-5), or specific jobs (1,3,4-6)
#SBATCH -t 4:00:00		## Maximum run time allowed dd-hh:mm:ss
#SBATCH -o <path_to_directory>/run_salmon.sh.%A_%a.stdout		## File in which to write errors and ouput to screen

# Script to build a salmon index for a file of transcript sequences
# Requires transcript file to be downloaded in advance
# Written to run on Compute Canada cluster (graham, cedar, etc.)
# Strings in angle brackets <> need to be replaced with paths and filenames for user's account

# Load required modules
module load nixpkgs/16.09
module load gcc/7.3.0
module load openmpi/3.1.2
module load salmon/1.1.0

SALMON_INDEX="<path_to_salmon_index_dir>/<index_name>"
FASTQ_DIR="<path_to_fastq_files>"
ENDEDNESS="SE"		# Is the RNA-seq data single-end (SE) or paired-end (PE)

# In the section below (lines 29-36) include a block for each sample for which you want to run salmon.
# When the script is queued, a copy of the script is launched for each value of --array at line 5
# That value is passed to the variable $SLURM_ARRAY_TASK_ID, which is used in turn to select the 
# values for $FQFILE and $SAMPLE from this section.

case "$SLURM_ARRAY_TASK_ID" in	
	"1" )
		FQFILE="<file1>.fastq.gz"		# Name of the fastq file in $FASTQ_DIR
		SAMPLE="<sample1>" ;;			# Name of the sample
	"2" )
		FQFILE="<file2>.fastq.gz"
		SAMPLE="<sample2>" ;;
esac

SALMON_DIR="<path_to_salmon_results>/$SAMPLE"

if [ ! -d "$SALMON_DIR" ]; then
	mkdir "$SALMON_DIR"
fi

# Run salmon quant - either single-end or paired-end
# If data are paired-end, need to modify the case statement at lines 29-36:
#
#	"1" )
#		FQFILE1="<fqfile1_R1>"
#		FQFILE2="<fqfile1_R2>"
#		SAMPLE="sample1" ##

if [ "$ENDEDNESS" == "SE" ]; then
	echo "salmon quant -p $SLURM_JOB_CPUS_PER_NODE -i $SALMON_INDEX -l A --gcBias --validateMappings -r $TMPDIR/$FQFILE -o $SALMON_DIR"
	salmon quant -p $SLURM_JOB_CPUS_PER_NODE -i $SALMON_INDEX -l A --gcBias --validateMappings -r $TMPDIR/$FQFILE -o $SALMON_DIR
elif [ "$ENDEDNESS" == "PE" ]; then
	echo "salmon quant -p $SLURM_JOB_CPUS_PER_NODE -i $SALMON_INDEX -l A --gcBias --validateMappings -1 $TMPDIR/$FQFILE1 -2 $TMPDIR/$FQFILE2 -o $SALMON_DIR"
	salmon quant -p $SLURM_JOB_CPUS_PER_NODE -i $SALMON_INDEX -l A --gcBias --validateMappings -1 $TMPDIR/$FQFILE1 -2 $TMPDIR/$FQFILE2 -o $SALMON_DIR
fi

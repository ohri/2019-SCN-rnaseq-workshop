# SCN RNASeq Workshop 2019: g:Profiler Tutorial

This tutorial will introduce you to the use of g:Profiler as an example of a web based gene annotation enrichment analysis tool.

Download:
* the [ordered list of up-regulated signficant genes](Files/Act_vs_Qui_up_ensembl.txt) from the Act vs Qui comparison
* the [ordered list of down-regulated signficant genes](Files/Act_vs_Qui_down_ensembl.txt) from the Act vs Qui comparison
* the [list of background genes](Files/Act_vs_Qui_all_ensembl.txt) from the Act vs Qui comparison 

Note that these are fold change lists of Activated vs Quiescent human satellite cells;

> Adult satellite cells are quiescent, but are poised for activation in response to exercise, injury, or disease allowing adult muscle growth or repair. Once activated, satellite cells proliferate extensively to produce enough myogenic progenitors in order to regenerate the muscles.
* _Brun, Caroline E., Yu Xin Wang, and Michael A. Rudnicki. "Single EDL myofiber isolation for analyses of quiescent and activated muscle stem cells." Cellular Quiescence. Humana Press, New York, NY, 2018. 149-159._


Go to the [g:Profiler site](https://biit.cs.ut.ee/gprofiler/gost)

# The effect of parameter changes on enrichment scores

In this exercise we perform a g:Profiler analysis on a differential gene expression set, 
changing various parameters and comparing the results to demonstrate the sensitivity of such programs to parameter differences.

* Click on the _Upload query_, then click on the upload query box and select the `Act_vs_Qui_up_ensembl.txt` file.
* These are Ensembl human gene identifiers so ensure that "Homo sapiens (Human)" as the organism; it should be the default.
* Ensure the `Ordered query` option is not selected; this means that the list will will be treated as an unordered list.
* Under data sources select only:
  * GO biological process
  * KEGG
* Click `Run query` and scroll to the results under the form
* Select the "Detailed Results" tab.
  * Note the terms listed both in the "GO:BP" table at the top and the "KEGG" table below it.
* Click on the "Term size" checkbox; enter `3` (min) in the left hand box and `500` (max) in the right hand box. 
  * How has this changed the output? Why?
* Click on CSV to save the ranked list as "unranked_no_background.txt"
* Go back up to the query options and select the "Advanced Options", select "Custom over all know genes" statistical domain scope. Open the `Ensembl_Act_vs_Qui_all_salmon.txt` file in a text editor, select all text, copy, then paste it into the text field below "Custom over all know genes".
* Click "Run query" and check the results again. Look at the prior results file and compare some of the term rankings? How have rankings and scores changed?
  * Click on CSV to save the ranked list as "unranked_background.txt"
* Select `ordered query` option
  * Click on CSV to save the ranked list as "ranked_background"
* We now have 3 files of results:
  * "unranked_no_background.txt"
  * "unranked_background.txt"
  * "ranked_background.txt"
* Open these in text editors or excel and compare the results
  * Select the first 10 ranked categories from the "unranked_no_background.txt" and compare to the other two files. How have the rankings and scores changed?
  * Do the enrichment categories you see makes sense for the cell types involved?


## Other annotation enrichment tools

There are other Annotation enrichmnent tools; [DAVID](https://david.ncifcrf.gov/) is one of the more popular. 
If you have time try to replicate some of the above results using DAVID.
 
* Go to [DAVID](https://david.ncifcrf.gov/)
* Click on the `Functional Annotation` tab 
* Select the `Step 1: Enter Gene List` file dialog and upload the file "Act_vs_Qui_up_ensembl.txt"
* In the `Step 2: Select Indentifier` box select `ENSEMBL_GENE_ID`
* In the `Step 3: List Type` box select Gene List
* Click `Submit List`
* Select Species `Homo sapiens` when prompted
* Click `Functional Annotation Chart` 

Compare these results to the g:Profiler results. 
How are they similar?
How are they different?
Can you figure out how to upload a custom background?




#!/bin/bash

#SBATCH -c 4			## The number of CPU cores to use. Stored in the variable $SLURM_JOB_CPUS_PER_NODE
#SBATCH --mem=8192		## Amount of memory to use in kb.
#SBATCH -t 4:00:00		## Maximum run time allowed dd-hh:mm:ss
#SBATCH -o <path_to_directory>/index_salmon.sh.%A_%a.stdout		## File in which to write errors and ouput to screen

# Script to build a salmon index for a file of transcript sequences
# Requires transcript file to be downloaded in advance
# Written to run on Compute Canada cluster (graham, cedar, etc.)
# Strings in angle brackets <> need to be replaced with paths and filenames for user's account

# Load required modules
module load nixpkgs/16.09
module load gcc/7.3.0
module load openmpi/3.1.2
module load salmon/1.1.0

cd $TMPDIR		## Work in the local temporary directory

TRANSCRIPT_FASTA="<path_to_transcript_dir>/<transcript_file>"
SALMON_INDEX="<path_to_salmon_index_dir>/<index_name>" ;;

## If the transcript file is compressed, uncompress it to $TMPDIR, 
## otherwise copy it to $TMPDIR

if [ ${TRANSCRIPT_FASTA: -3} == ".gz" ]; then
	gunzip -c $TRANSCRIPT_FASTA > transcripts.fa
else
	cp $TRANSCRIPT_FASTA ./transcripts.fa
fi

## Index the transcript file

salmon index --gencode -p $SLURM_JOB_CPUS_PER_NODE -t transcripts.fa -i $SALMON_INDEX	
